import { Component, OnInit } from '@angular/core';
import { WindowRef } from '../WindowRef.service';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  loading = true;
  businessName: string;
  amount: string;
  vehicleNumber: string;
  inTime: any;


  constructor(private winRef: WindowRef, private httpClient: HttpClient) {
  }

  // data-key="<YOUR_KEY_ID>"
  //   data-amount="5000"
  //   data-buttontext="Pay with Razorpay"
  //   data-name="Merchant Name"
  //   data-description="Purchase Description"
  //   data-image="https://your-awesome-site.com/your_logo.jpg"
  //   data-prefill.name="Gaurav Kumar"
  //   data-prefill.email="test@test.com"
  //   data-theme.color="#F37254"

  rzp1: any;


  getData() {
    this.httpClient.get('http://www.mocky.io/v2/5c0a66033500006a00a8611f').toPromise()
      .then(response => {
        console.log('Check Response', response);
        const jsonStr = JSON.stringify(response);
        const jsonObj = JSON.parse(jsonStr);
        const data = jsonObj['data'];
        console.log('Json Response', data);
        this.businessName = data['businessName'];
        this.amount = data['amount'];
        console.log(this.amount);

        this.inTime = data['inTime'];
        this.vehicleNumber = data['vehicleNumber'];
        this.loading = false;

      }
      );

    // http://www.mocky.io/v2/5c090bf72f0000eb37637c01
  }
  ngOnInit() {
    this.getData();
    // this.loader = false;
  }

  public initPay(): void {
    const options = {
      'key': 'rzp_live_6W0QseJ0jPcupv',
      'amount': '100',
      'name': 'Get Parking',
      "handler": function (response) {
        console.log(response);
        alert(response.razorpay_payment_id)
      },
      "prefill": {
        "name": "Ami Kumar",
        "email": "ami.petu.kumar@test.com",
        "phone": '809780454'
      },
    };
    this.rzp1 = new this.winRef.nativeWindow.Razorpay(options);
    this.rzp1.open();
  }

}
